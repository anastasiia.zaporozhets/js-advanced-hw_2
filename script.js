"use strict"

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const div = document.createElement("div");
div.setAttribute("id", "root");
document.body.prepend(div);

const ul = document.createElement("ul");
div.append(ul);


function displayBooks() {
    books.forEach((book) => {
        try {
            if (!book.author) {
                throw new Error(`Помилка! НЕМАЄ  author`);
            }
            if (!book.name) {
                throw new Error(`Помилка! НЕМАЄ name`);
            }
            if (!book.price) {
                throw new Error(`Помилка! НЕМАЄ price`);
            }

            const li = document.createElement("li");
            li.textContent = `Автор: ${book.author}, Назва книги: ${book.name}, Ціна: ${book.price} грн.`;
            ul.append(li);
        } catch (error) {
            if (error instanceof Error) {
                console.error(error.message);
            } else {
                throw error;
            }
        }
    });
}

displayBooks();










